var socket = io('/api/socket.io');
console.info('client.js connected');

socket.on('news', function (data) {
    console.log(data);
    socket.emit('my other event', { my: 'data' });
});