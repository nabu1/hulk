$(function() {
    var source = new EventSource('/stream');
    source.addEventListener('test-express', handleMessage, false);
    source.onmessage = handleMessage;
    console.info('Subscribe on test-express');
    function handleMessage(e) {
        console.log(e);
    }
});