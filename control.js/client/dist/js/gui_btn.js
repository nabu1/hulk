$(function() {
    $('.ctrl').each(function (i, ctrl) {
        $.each(Object.keys($(ctrl).data('msg')), function(i, eventName) {
            console.debug("Set "+eventName+" event hander for "+$(ctrl));
            $(ctrl).on(eventName, function (e) {
                console.info($(e.currentTarget).data('msg'));
                console.info($(ctrl).data('msg')[eventName]);
                $.ajax({
                    type: "POST",
                    url: "/send_message",
                    data: JSON.stringify( {"message": $(ctrl).data('msg')[eventName]} ),
                    dataType: 'json',
                    contentType: "application/json; charset=utf-8",
                })
            })
        })
        console.info($(ctrl).data('msg'));
    })
});