'use strict'

/**
 * Module dependencies
 */

const express = require('express');
const fs = require('fs');
const path = require('path');
const Redis = require('ioredis');
const  bodyParser = require('body-parser')
/**
 * To test, publish something on 'test-express' channel
 *
 * $ redis-cli publish test-express testmessage
 */


// stream

const subscribe = require('redis-subscribe-sse')


// express app
const clientRoot = path.join(process.cwd(), '../client/dist');

let app = express()

app.get('/', (req, res) => {
    res.sendFile(`${clientRoot}/index.html`)
});

app.use(express.static(clientRoot));
app.use(bodyParser.json());

app.get('/move', (req, res) => {
    var redis = new Redis();
    redis.publish("test-express", "Hello world!");
    console.log('dfsfs');
    res.json({"foo": "bar"});
});

app.post('/send_message', (req, res) =>{
    var message = req.body.message;
    console.log(message);
    var redis = new Redis();
    redis.publish("test-express", JSON.stringify(message));
    res.json('ack')
})

app.get('/stream', (req, res) => {
  let sse = subscribe({
    channels: 'test-express',
    retry: 5000,
    host: '127.0.0.1',
    port: 6379
  })

  req.socket.setTimeout(Number.MAX_VALUE)

  res.set({
    'Content-Type': 'text/event-stream',
    'Cache-Control': 'no-cache',
    'Connection': 'keep-alive'
  })

  sse.pipe(res)
})

app.listen(3000, () => {
  console.log('Express listening on port 3000')
})
