import threading
import redis
from devices.L293D.ASMpi import AMSpi as MotorShield

class Listener(threading.Thread):
    def __init__(self, r, p):
        threading.Thread.__init__(self)
        self.redis = r
        self.pubsub = self.redis.pubsub()
        self.pubsub.psubscribe(p)

    def run(self):
        for m in self.pubsub.listen():
            if 'pmessage' != m['type']:
                pass
            print ('[{}]: {}'.format(m['channel'], m['data']))

if __name__ == "__main__":
    motors = MotorShield()
    r = redis.StrictRedis()
    client = Listener(r, '*')
    client.start()

    r.publish('channel1', 'message1')
    print ('Main ended.')